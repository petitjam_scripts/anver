# `anver`

Simple script to check Android version names/API levels.

Enter in a release name, number, or API level and it will give you the full details for that release.

```sh
$ anver 12
Name: Android Honeycomb, Internal Code Name: Honeycomb, Version: 3.1, Api Level: 12, Release Date: 2011-05-10
Name: Android 12, Internal Code Name: Snow Cone, Version: 12, Api Level: 31, Release Date: 2021-10-04
Name: Android 12L, Internal Code Name: Snow Cone v2, Version: 12.1, Api Level: 32, Release Date: 2022-03-07

$ anver oreo
Name: Android Oreo, Internal Code Name: Oatmeal Cookie, Version: 8.0, Api Level: 26, Release Date: 2017-08-21
Name: Android Oreo, Internal Code Name: Oatmeal Cookie, Version: 8.1, Api Level: 27, Release Date: 2017-12-05

$ anver 14
Name: Android Ice Cream Sandwich, Internal Code Name: Ice Cream Sandwich, Version: 4.0, Api Level: 14, Release Date: 2011-10-18
Name: Android Ice Cream Sandwich, Internal Code Name: Ice Cream Sandwich, Version: 4.0.1, Api Level: 14, Release Date: 2011-10-21
Name: Android Ice Cream Sandwich, Internal Code Name: Ice Cream Sandwich, Version: 4.0.2, Api Level: 14, Release Date: 2011-11-28
Name: Android 14, Internal Code Name: Upside Down Cake, Version: 14, Api Level: 34, Release Date: Q3 2023
```
